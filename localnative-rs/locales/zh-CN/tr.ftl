# 侧边栏
notes = 笔记
sync = 同步

# 搜索页面
range = 搜索范围从
to = 到
search = 输入搜索内容...
tags = 标签:

# 同步页面
sync-waiting = 等待操作...
sync-syncing = 正在尝试同步...
sync-error = 同步过程出错:
sync-complete = 同步过程已完成！
sync-ip-parse-error = 您输入的 IP 地址格式有误，请检查。
sync-ip-parse-complete = 您输入的 IP 地址格式正确。
sync-file-path-error = 所选文件路径不正确，请重新选择文件。
sync-from-file-unknown-error = 文件同步过程中发生未知错误。您要同步的文件可能不正确。

sync-client-tip = 输入 IP 地址和端口号，您的设备将连接到其他设备的同步服务。
sync-server-tip = 启用同步服务后，其他设备上的本地应用程序将能够同步您的设备。
input-ip-tip = 如果您要输入 IPv6 地址，可以通过键盘快捷键将其复制并粘贴到 IP 输入框中。
input-ip = IP 地址和端口号：
sync-from-server = 从服务器同步到本地
sync-to-server = 从本地同步到服务器
sync-from-file = 从文件同步到本地

closed = 本地服务已关闭
opened = 本地服务已开启
starting = 正在启动本地服务
closing = 正在关闭本地服务
unknow-error = 未知错误，请点击重试
ip-qr = 使用需要同步的设备上的 Local Native 应用程序，扫描 QR 码进行同步，或手动输入：{$ip} 进行同步。

settings = 设置
disable-delete-tip = 删除时不进行提示，直接删除。
language = 语言
chinese = 中文
english = 英语(English)
limit = 搜索结果每页的最大数量
ok = 确定
cancel = 取消

delete-tip = 警告
delete-tip-content = 删除后将无法恢复，请谨慎操作！

not-found = 抱歉，未找到您要的结果！
nothing = 您尚未创建标签，您可以从其他设备同步到此设备，或从浏览器扩展程序添加新标签。

try-fix-host = 您的浏览器无法正常通信？尝试单击此处修复。

sync-file-title = 选择您将用于同步的 sqlite3 文件

sync-file = 用于同步的 *.sqlite3 文件

date = 日期
count = 数量