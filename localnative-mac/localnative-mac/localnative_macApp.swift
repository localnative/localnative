//
//  localnative_macApp.swift
//  localnative-mac
//
//  Created by Yi Wang on 7/9/21.
//

import SwiftUI

@main
struct localnative_macApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
