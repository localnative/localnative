﻿/*
    Local Native
    Copyright (C) 2018-2019  Yi Wang

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
let locales = {};

locales['zh-CN'] = i18n.create({
  values: {
    "ssbify": '保存内容到 ssb',
    "public": '公开',
    "Language": '语言',
    "Sync Between Devices": '同步设备',
    "sync via attach file": '通过文件同步',
    "Server": '服务器',
    "start server": '启动服务器',
    "stop server": '关闭服务器',
    "Client": '客户端',
    "start client sync": '启动客户端同步',
    "description": "描述",
    "title": '标题',
    "url": 'url 链接',
    "type to add tags, enter to save, comma or space as tag seperator": '输入标签，回车保存，半角逗号或空格为标签的分隔符',
    "type to search": '输入搜索',
    "clear search term(s)": '清除搜索框',
    "prev": '上页',
    "next": '下页',
    "page": '页',
    "of": '共',
    "ssb sync": 'ssb 同步',
    "Tags": '标签',
    "Timeseries Charts": '时间序列图',
    "select a time range to zoom in": '选择要放大的时间范围'
  }
});
